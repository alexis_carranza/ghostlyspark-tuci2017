# module PaymentsHelper
#   def find_days_till_payment(payments)
#     payments.each do |payment|
#       payment.days_till_payment = payment.due_date - (DateTime.now.to_date - 30)
#       payment.save
#     end
#   end
# end

class Api::PaymentsController < ApplicationController
  def show
    @payments = Payment.all
    @payments.each do |payment|
      payment.days_till_payment = payment.due_date - (DateTime.now.to_date - 30)
      payment.save
    end
    render json: @payments.order(days_till_payment: :desc)
  end
end
