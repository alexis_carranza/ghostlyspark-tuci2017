class Api::CreditDashboardsController < ApplicationController
  def show
    @user = Person.find_by(UID: params[:id])

    credit_score = @user.score
    total_accts = @user.num_accts
    years_history = @user.months_on_file/12
    derogatory_marks = @user.num_accts_over_30days_past_due + @user.num_accts_over_60days_past_due + \
                      @user.num_accts_over_90days_past_due + @user.num_accts_over_120days_past_due + \
                      @user.num_collection_inquires
    hard_inquiries = @user.num_inquires

    cc_utilization = @user.total_balance_all_accts.gsub(',', '').gsub(/[^0-9.]/, '').to_f / @user.total_high_credit_limit.gsub(',', '').gsub(/[^0-9.]/, '').to_f
    total_credit_limit = @user.total_high_credit_limit.gsub(',', '').gsub(/[^0-9.]/, '').to_f

    response = {
      "UID": @user.UID,
      "credit_score": credit_score,
      "total_accts": total_accts,
      "years_history": years_history,
      "derogatory_marks": derogatory_marks,
      "hard_inquiries": hard_inquiries,
      "cc_utilization": cc_utilization,
      "total_credit_limit": total_credit_limit,
    }
    render json: response.to_json
  end
end
