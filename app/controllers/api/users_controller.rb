class Api::UsersController < ApplicationController

  def index
    @users = Person.all
    render json: @users
    # @users = User.all
    # render json: @users
  end

  def show
    @user = Person.find_by(UID: 1)
    render json: @user
    # @user = User.find(params[:id])
    # render json: @user
  end
end
