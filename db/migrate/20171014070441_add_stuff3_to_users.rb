class AddStuff3ToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :num_active_personal_finance_accts, :integer
    add_column :users, :months_oldest_personal_finance_acct_opened, :integer
    add_column :users, :months_newest_personal_finance_acct_opened, :integer
    add_column :users, :num_personal_finance_acct_balance, :integer
    add_column :users, :num_retail_accts, :integer
    add_column :users, :num_active_retail_accts, :integer
    add_column :users, :months_oldest_retail_acct_opened, :integer
    add_column :users, :months_newest_retail_acct_opened, :integer
    add_column :users, :num_retail_acct_balances, :integer
    add_column :users, :num_open_accts, :integer
    add_column :users, :num_open_revolving_accts, :integer
    add_column :users, :num_accts_over_30days_past_due, :integer
    add_column :users, :num_accts_over_60days_past_due, :integer
    add_column :users, :num_accts_over_90days_past_due, :integer
    add_column :users, :num_accts_over_120days_past_due, :integer
    add_column :users, :num_collection_inquires, :integer
    add_column :users, :total_high_credit_limit, :string
    add_column :users, :total_balance_all_accts, :string
    add_column :users, :avg_balance_all_accts, :string
    add_column :users, :percent_bankcards_over_50_percent_of_limit, :string
    add_column :users, :num_inquires, :integer
    add_column :users, :months_since_recent_inquiry, :integer
    add_column :users, :num_public_record_bankruptcies, :integer
    add_column :users, :score, :integer
  end
end
