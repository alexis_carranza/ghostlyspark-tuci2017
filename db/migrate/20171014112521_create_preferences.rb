class CreatePreferences < ActiveRecord::Migration[5.1]
  def change
    create_table :preferences do |t|
      t.integer :pawpoints
      t.integer :clothing_bitmask
      t.integer :pets_bitmask
      t.integer :friends_count

      t.timestamps
    end
  end
end
