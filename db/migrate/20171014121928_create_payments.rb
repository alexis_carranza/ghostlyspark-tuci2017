class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.integer :UID
      t.date :paid_date
      t.string :account
      t.integer :amount

      t.timestamps
    end
  end
end
