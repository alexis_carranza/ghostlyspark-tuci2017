class AddStuff1ToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :months_oldest_acct_opened, :integer
    add_column :users, :months_newest_acct_opened, :integer
    add_column :users, :accts_with_balances, :integer
    add_column :users, :num_bankcard_acct, :integer
    add_column :users, :num_active_bankcard_acct, :integer
    add_column :users, :months_oldest_bankcard_opened, :integer
    add_column :users, :months_newest_bankcard_opened, :integer
    add_column :users, :num_bankcards_with_balances, :integer
  end
end
