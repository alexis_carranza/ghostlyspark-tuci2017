class AddDaysTillPaymentToPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :days_till_payment, :integer
  end
end
