class AddUidToPreference < ActiveRecord::Migration[5.1]
  def change
    add_column :preferences, :UID, :integer
  end
end
