class AddStuffToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :telephone, :string
    add_column :users, :age, :integer
    add_column :users, :num_accts, :integer
  end
end
