class AddDueDateMonthStringDueDateDayStringToPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :due_date_month_string, :string
    add_column :payments, :due_date_day_string, :string
  end
end
