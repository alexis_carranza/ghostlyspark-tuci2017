class AddStuff2ToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :num_installment_accts, :integer
    add_column :users, :months_oldest_bank_install_acct_opened, :integer
    add_column :users, :num_finance_acct, :integer
    add_column :users, :months_since_activity, :integer
    add_column :users, :months_on_file, :integer
    add_column :users, :num_active_installment_accts, :integer
    add_column :users, :months_oldest_install_acct_opened, :integer
    add_column :users, :months_newest_install_acct_opened, :integer
    add_column :users, :num_install_accts_balances, :integer
    add_column :users, :num_mortgage_accts, :integer
    add_column :users, :num_active_mortgage_accts, :integer
    add_column :users, :months_oldest_mortgage_opened, :integer
    add_column :users, :months_newest_mortgage_opened, :integer
    add_column :users, :num_mortgage_accts_balance, :integer
    add_column :users, :num_personal_finance_accts, :integer
  end
end
