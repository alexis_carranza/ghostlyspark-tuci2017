# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171014170242) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "payments", force: :cascade do |t|
    t.integer "UID"
    t.date "paid_date"
    t.string "account"
    t.integer "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "due_date"
    t.string "due_date_month_string"
    t.string "due_date_day_string"
    t.integer "days_till_payment"
  end

  create_table "preferences", force: :cascade do |t|
    t.integer "pawpoints"
    t.integer "clothing_bitmask"
    t.integer "pets_bitmask"
    t.integer "friends_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "UID"
  end

  create_table "private_data", force: :cascade do |t|
    t.integer "pawpoints"
    t.integer "clothing_bitmask"
    t.integer "pets_bitmask"
    t.integer "friends_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer "UID"
    t.string "title"
    t.string "first_name"
    t.string "last_name"
    t.string "street_address"
    t.string "city"
    t.string "state"
    t.string "zipcode"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "telephone"
    t.integer "age"
    t.integer "num_accts"
    t.integer "months_oldest_acct_opened"
    t.integer "months_newest_acct_opened"
    t.integer "accts_with_balances"
    t.integer "num_bankcard_acct"
    t.integer "num_active_bankcard_acct"
    t.integer "months_oldest_bankcard_opened"
    t.integer "months_newest_bankcard_opened"
    t.integer "num_bankcards_with_balances"
    t.integer "num_installment_accts"
    t.integer "months_oldest_bank_install_acct_opened"
    t.integer "num_finance_acct"
    t.integer "months_since_activity"
    t.integer "months_on_file"
    t.integer "num_active_installment_accts"
    t.integer "months_oldest_install_acct_opened"
    t.integer "months_newest_install_acct_opened"
    t.integer "num_install_accts_balances"
    t.integer "num_mortgage_accts"
    t.integer "num_active_mortgage_accts"
    t.integer "months_oldest_mortgage_opened"
    t.integer "months_newest_mortgage_opened"
    t.integer "num_mortgage_accts_balance"
    t.integer "num_personal_finance_accts"
    t.integer "num_active_personal_finance_accts"
    t.integer "months_oldest_personal_finance_acct_opened"
    t.integer "months_newest_personal_finance_acct_opened"
    t.integer "num_personal_finance_acct_balance"
    t.integer "num_retail_accts"
    t.integer "num_active_retail_accts"
    t.integer "months_oldest_retail_acct_opened"
    t.integer "months_newest_retail_acct_opened"
    t.integer "num_retail_acct_balances"
    t.integer "num_open_accts"
    t.integer "num_open_revolving_accts"
    t.integer "num_accts_over_30days_past_due"
    t.integer "num_accts_over_60days_past_due"
    t.integer "num_accts_over_90days_past_due"
    t.integer "num_accts_over_120days_past_due"
    t.integer "num_collection_inquires"
    t.string "total_high_credit_limit"
    t.string "total_balance_all_accts"
    t.string "avg_balance_all_accts"
    t.string "percent_bankcards_over_50_percent_of_limit"
    t.integer "num_inquires"
    t.integer "months_since_recent_inquiry"
    t.integer "num_public_record_bankruptcies"
    t.integer "score"
  end

end
